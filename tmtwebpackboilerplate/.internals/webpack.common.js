const path = require('path')
const paths = require('./paths');
const fs = require('fs');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');

const getHTMLPluggedFiles = dir => {
    const files = fs.readdirSync(path.resolve(__dirname, dir))
    const templates = [];
    
    files.map(file => {
        const parts = file.split('.')
        const name = parts[0]
        const extension = parts.reverse()[0]
        if( extension !== 'html' ) return false;
        const args = {
            favicon: paths.src + '/assets/images/favicon.png',
            filename: `${name}.${extension}`,
            template: path.resolve(__dirname, `${dir}/${name}.${extension}`),
            inject: true
        }
        if( name === 'index' ) args.inject = false;

        templates.push(new HTMLWebpackPlugin(args))      
    })

    return templates;
}

const htmlPlugins = getHTMLPluggedFiles(paths.public)

module.exports = {
    entry: [
        paths.src + '/index.js'
    ],
    output: {
        path: paths.build,
        filename: '[name].bundle.js',
        publicPath: '/',
    },
    plugins: [
        new CleanWebpackPlugin(),
    ].concat(htmlPlugins),
    module: {
        rules: [
            {
                test: /\.js$/, 
                exclude: /node_modules/, 
                use: ['babel-loader']
            },
            {
                test: /\.(?:ico|gif|png|jpg|jpeg)$/i, 
                type: 'asset/resource'
            },
            {
                test: /\.(woff(2)?|eot|ttf|otf|svg|)$/, 
                type: 'asset/inline'
            }           
        ]
    }
}