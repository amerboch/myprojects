# TMT Gulp 1.2.0 :100:
> Front-end gulp based boilerplate, made by TMT.

### CHANGELOG
 - Upgraded to Gulp 4, Nodejs 12+ :fingers_crossed:
 - deprecating compass, in favor of postcss and autoprefixer :bomb:
 - Implemented Stylelint, with strict configuration for clean code improvements :fist:
 - CSSNANO :bomb:
 - Modular tasks and helpers ( check ./.internals/tasks/ ) :bomb:.
 - Implemented Bitbucket Pipelines, autodeployment and build on the cloud :cloud:

## les pré-requis
 - Node >= 12
 - gulp >=4

### Outils et méthodes optionnels

 - Bower
 - bower-installer ( npm install -g bower-installer / yarn global add bower-installer )

## Méthode d'installation
Pour installer le projet, il sufit d'abord de faire installer les pré-requis, et suivre la liste des commandes suivante

##
##### En utilisent `NPM`
##

```sh
$ npm install
$ npm start
```
##
##### En utilisent `YARN`
##

```sh
$ yarn install
$ yarn start
```