"use strict";
/*
//
//	d888888b .88b  d88. d888888b    d888b  db    db db      d8888b.    db    .d888b. 
//	`~~88~~' 88'YbdP`88 `~~88~~'   88' Y8b 88    88 88      88  `8D   o88    VP  `8D 
//	   88    88  88  88    88      88      88    88 88      88oodD'    88       odD' 
//	   88    88  88  88    88      88  ooo 88    88 88      88~~~      88     .88'   
//	   88    88  88  88    88      88. ~8~ 88b  d88 88booo. 88         88 db j88.    
//	   YP    YP  YP  YP    YP       Y888P  ~Y8888P' Y88888P 88         VP VP 888888D 
//
//   @AUTHOR : SAID ASSEMLAL
//
//   * 1. yarn install / npm install
//   * 2. bower-installer ( bower install works too, but I'd prefer this to avoid manual copying …  )
//   * 3. yarn start / npm start
//
*/

const gulp = require('gulp');
const browsersync = require('./.internals/browsersync/index.js');
const clean = require('./.internals/tasks/clean.js');
const sass = require('./.internals/tasks/sass.js');
const js = require('./.internals/tasks/js.js');
const images = require('./.internals/tasks/images.js');
const sprites = require('./.internals/tasks/sprites.js');
const htmlshiv = require('./.internals/tasks/htmlshiv.js');
const fonts = require('./.internals/tasks/fonts.js');
const files = require('./.internals/tasks/files.js');
const html = require('./.internals/tasks/html.js');
const config = require('./.internals/config.js');

// @watch
// @todo : Trigger gulp's watch task, surveil different folders / files and wait for changes
const watch = cb => {

	gulp.watch('src/assets/images/pictos/*.png', gulp.series(sprites));
	gulp.watch('src/sass/**/*.scss', gulp.series(sass));
	gulp.watch(['src/assets/images/**/*.*', 'src/assets/images/*.*', '!src/assets/images/pictos/*.*'], gulp.series(images));
	gulp.watch(['src/basePath/*.*', 'src/basePath/**/*.*'], gulp.series(html));
	gulp.watch(['src/assets/js/*.js', 'src/assets/js/**/*.js'], gulp.series(js));	
	// Optional tasks
	if( config.hasFonts ) gulp.watch('src/assets/fonts/**/*.*', gulp.series(fonts));
	if( config.hasFiles ) gulp.watch(['src/assets/files/**/*.*', 'src/assets/files/*.*'], gulp.series(files));
}

// @Build
// @todo : Generates build folder, by triggering a serie of tasks 
//         to generate the assets( js, css, files & fonts ), html pages
const build = gulp.series(clean, gulp.series(sprites, gulp.parallel(
	sass, 
	js,
	images, 
	fonts, 
	files,
	html
)));

build.description = 'Generates build folder, by triggering a serie of tasks to generate the assets( js, css, files & fonts ), html pages';

exports.default = build;
exports.clean = clean;
exports.build = build;
exports.html = html;
exports.js = js;
exports.sass = sass;
exports.images = images;
exports.fonts = fonts;
exports.files = files;
exports.watch = gulp.series(build, gulp.parallel(watch, browsersync.init));