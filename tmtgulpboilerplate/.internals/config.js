const hasFiles = false;
const hasFonts = true;
const jsFile = 'main.min.js';
const hasDeploy = false;
const buildFolder = 'build/';
const assetsPath = buildFolder + '/assets';
const browsersyncOptions = {
    stream: true,
    ghostMode: false
}

module.exports = {
	hasFiles,
	hasFonts,
	jsFile,
	hasDeploy,
	buildFolder,
	assetsPath,
	browsersyncOptions
}