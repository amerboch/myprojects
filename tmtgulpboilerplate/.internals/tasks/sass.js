const gulp = require('gulp');
const gulpsass = require('gulp-sass');
const callback = require('gulp-callback');
const postcss = require('gulp-postcss');
const stylelint = require('gulp-stylelint');
const notifications = require('../helpers/notifications.js');
const plumber = require('gulp-plumber');
const autoprefixer = require('autoprefixer');
const deployAssets = require('../helpers/assets.js');
const browsersync = require('../browsersync/index.js');
const cssnano = require('cssnano');
const config = require('../config.js');

// @sass
// @todo : handles sass watcher, and compiler using the gulp-sass modules
//         with AutoPrefixer and StyleLint implementation
const sass = () => {
	const processors = [
		autoprefixer(),
		cssnano(),
	];
	return gulp.src([
			'src/sass/*.scss',
			'src/sass/**/*.scss'
		], { allowEmpty : true})
		.pipe(plumber(notifications))
		.pipe(stylelint({
			fix : true,
			reporters: [
				{
					formatter: 'string', 
					console: true
				}
			]
		}))		
		.pipe(gulpsass({ outputStyle: 'compressed' }))
		.pipe(postcss(processors))
		.pipe(gulp.dest(config.assetsPath+'/css'))
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}))	
		.pipe(browsersync.reload())
};

sass.description = 'handles sass watcher, and compiler using the gulp-sass modules with AutoPrefixer and StyleLint implementation';

module.exports = sass;