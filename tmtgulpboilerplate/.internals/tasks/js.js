const gulp = require('gulp');
const concat = require('gulp-concat');
const path = require('path');
const uglify = require('gulp-uglify');
const callback = require('gulp-callback');
const deployAssets = require('../helpers/assets.js');
const notifications = require('../helpers/notifications.js');
const plumber = require('gulp-plumber');
const config = require('../config.js');
const browsersync = require('../browsersync/index.js');
const fs = require('fs');


// @gatherDependencies
// @todo : gathers dependencies by order, from the dependencies.json file.
const gatherDependencies = async () => {
	let deps = [];
	return new Promise((resolve) => {
		fs.readFile('./dependencies.json', 'utf8', (err, data) => {
		    var obj = JSON.parse(data);
		    
		    if( ! obj.deps ) return;

		    obj.deps.map( dep => {   
				if( fs.existsSync('./' + dep) ){
					deps.push(dep)
				}else{
					 console.warn('does not exist')
				}
		    });

		    resolve(deps);
		});			
	})
}

// @js
// @todo : Handles JS watcher with Uglify and Concat implementation
const js = async () => {
	const deps = await gatherDependencies();
	return gulp.src(deps, { allowEmpty : true})
		.pipe(plumber(notifications))
		.pipe(concat({ path: config.jsFile, stat: { mode: '0666' }}))
		.pipe(uglify())
		.pipe(gulp.dest(config.assetsPath+'/js'))
		.pipe(browsersync.reload())
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}))
		
};
js.description = 'Handles JS watcher with Uglify and Concat implementation';


module.exports = js;