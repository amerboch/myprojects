const gulp = require('gulp');
const gulpclean = require('gulp-clean');
const config = require('../config.js');

// @clean
// @todo : Clean build folder, removes everything for a fresh re-build
const clean = () => {
	return gulp.src(config.buildFolder, { read : false, allowEmpty : true})
	.pipe(gulpclean());
}
clean.description = 'Clean build folder, removes everything for a fresh re-build';

module.exports = clean;