const gulp = require('gulp');
const uglify = require('gulp-uglify');
const config = require('../config.js');

// @htmlshiv
// @todo : Copies the htmlshiv pollyfill into `./build/assets/js`
const htmlshiv = () => {
	return gulp.src(['src/assets/js/vendor/html5shiv/html5shiv.js'], { read : false, allowEmpty : true})
		.pipe(uglify())
		.pipe(gulp.dest(config.assetsPath+'/js'));
};
htmlshiv.description = 'Copies the htmlshiv pollyfill into `./build/assets/js`';


module.exports = htmlshiv;