const gulp = require('gulp');
const callback = require('gulp-callback');
const config = require('../config.js');
const deployAssets = require('../helpers/assets.js');
const browsersync = require('../browsersync/index.js');

// @html
// @todo : Deploy html pages / JSON files / htaccess & other base assets to the web folder
const html = () => {
	return gulp.src(['src/basePath/*.*', 'src/basePath/**/*.*'], { allowEmpty : true})
		.pipe(gulp.dest(config.buildFolder))
		.pipe(browsersync.reload())
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}))
};
html.description = 'Deploy html pages / JSON files / htaccess & other base assets to the web folder';

module.exports = html;