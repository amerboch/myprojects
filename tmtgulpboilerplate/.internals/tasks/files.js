const gulp = require('gulp');
const callback = require('gulp-callback');
const config = require('../config.js');
const deployAssets = require('../helpers/assets.js');
// @files
// @todo : Deploys static files such as PDFs, Videos …
const files = () => {
	return gulp.src(['src/assets/files/*.*'], { allowEmpty : true})
		.pipe(gulp.dest(config.assetsPath+'/files'))
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}));
}
files.description = 'Deploys static files such as PDFs, Videos …';

module.exports = files;