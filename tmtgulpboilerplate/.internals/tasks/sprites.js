const gulp = require('gulp');
const spritesmith = require('gulp.spritesmith');
const notifications = require('../helpers/notifications.js');
const plumber = require('gulp-plumber');
const imagemin = require('gulp-imagemin');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const callback = require('gulp-callback');
const config = require('../config.js');

// @sprites
// @todo : Sprites handler, takes the png pictos placed within the `./src/assets/images/pictos`
//         And merges em to make a sprite.png, with a SCSS file that handles the CSS part
const sprites = () => {
	// Generate our spritesheet
	const spriteData = gulp.src(['./src/assets/images/pictos/*.png'], { allowEmpty : true }).pipe(spritesmith({
			imgName: 'sprite.png',
			cssName: '_sprites.scss',
			imgPath: '../images/sprite.png'
	}));

	// Pipe image stream through image optimizer and onto disk
	const imgStream = spriteData.img
	// DEV: We must buffer our stream into a Buffer for `imagemin`
		.pipe(plumber(notifications))	
		.pipe(buffer())
		.pipe(imagemin())
		.pipe(gulp.dest(config.assetsPath+'/images'));

	// Pipe CSS stream through CSS optimizer and onto disk
	const cssStream = spriteData.css
		.pipe(plumber(notifications))	
		.pipe(gulp.dest('./src/sass/inc'));

	// Return a merged stream to handle both `end` events
	return merge(imgStream, cssStream);
}

sprites.description = 'Sprites handler, takes the png pictos placed within the `./src/assets/images/pictos` And merges em to make a sprite.png, with a SCSS file that handles the CSS part';

module.exports = sprites;