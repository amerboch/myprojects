const gulp = require('gulp');
const notifications = require('../helpers/notifications.js');
const plumber = require('gulp-plumber');
const imagemin = require('gulp-imagemin');
const callback = require('gulp-callback');
const config = require('../config.js');
const deployAssets = require('../helpers/assets.js');
// @images
// @todo : Handles static images, by minifying and deploying em into the build folder
const images = () => {
	return gulp.src(['src/assets/images/**/*.*', 'src/assets/images/*.*', '!src/assets/images/pictos/*.*'], { allowEmpty : true})
		.pipe(plumber(notifications))
		.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}]
		}))
		.pipe(gulp.dest(config.assetsPath+'/images'))
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}))
}
images.description = 'Handles static images, by minifying and deploying em into the build folder';


module.exports = images;

