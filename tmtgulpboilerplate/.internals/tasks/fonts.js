const gulp = require('gulp');
const callback = require('gulp-callback');
const browsersync = require('../browsersync/index.js');
const config = require('../config.js');
const deployAssets = require('../helpers/assets.js');
// @fonts
// @todo : Deploys web-fonts, by copying em' into `src/assets/fonts`
const fonts = () => {
	return gulp.src(['src/assets/fonts/*.*', 'src/assets/fonts/**/*.*'], { allowEmpty : true})
		.pipe(gulp.dest(config.assetsPath+'/fonts'))
		.pipe(browsersync.reload())
		.pipe(callback(function() {
			deployAssets(config.hasDeploy);
		}))
};
fonts.description = 'Deploys web-fonts, by copying em\' into `src/assets/fonts`';

module.exports = fonts;