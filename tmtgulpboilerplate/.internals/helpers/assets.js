const gulp = require('gulp');
const config = require('../config.js');

// @deployAssets
// @todo : Dependencies handler, takes the dependencies from dependencies.json
//         And manages to merge em all, into main.js
const deployAssets = deployPath => {
	if( deployPath != false ) return gulp.src([config.buildFolder + '/assets/**/*']).pipe(gulp.dest(deployPath));
	return false;
}
deployAssets.description = 'Dependencies handler, takes the dependencies from dependencies.json And manages to merge em all, into main.js';


module.exports = deployAssets;