const notify = require('gulp-notify');
const path = require('path');
// @notifyInfo & @plumberErrorHandler
// @todo : Custom notifications handler
// # Desktop notifications UI settings
const notifyInfo = {
	title: 'TMTGulp : An error has occurred !',
	icon: path.join( __dirname, 'tmt.png' )
};
// # Desktop notifications plumber
const plumberErrorHandler = {
		errorHandler: notify.onError({
			title: notifyInfo.title,
			icon: notifyInfo.icon,
			message: "Error: <%= error.message %>"
		})
};

module.exports = plumberErrorHandler;