const browsersync = require('browser-sync').create();
const config = require('../config.js');

const browsersyncInit = () => browsersync.init({
	notify : false,
	ui : false,
	server: {
		baseDir: config.buildFolder
	}
});

const autoReload = () => browsersync.reload(config.browsersyncOptions)

module.exports = {
	init : browsersyncInit,
	reload : autoReload,
	// stream : autoStream
};